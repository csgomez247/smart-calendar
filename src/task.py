class Task(object):
	'''class for Tasks, which hold lists of other Tasks'''

	def __init__(self, name, time_estimate, depth):
		'''initializes the name, time_estimate, and the depth of the task'''
		self.name = name
		self.time_estimate = time_estimate
		self.depth = depth
		self.subtasks = []

	def append_subtask(self, name, time_estimate):
		'''adds a subtask'''
		self.subtasks.append(Task(name, time_estimate, self.depth + 1))

	def get_depth(self):
		'''returns depth of current task'''
		return self.depth

	def get_subtask(self, num):
		'''returns a specified subtask'''
		return self.subtasks[num]

	def get_task_tree(self, tasks):
		'''recursively appends all tasks in current root-tree into a list named tasks'''
		tasks.append(self)
		if not self.subtasks:
			return
		for task in self.subtasks:
			task.get_task_tree(tasks)

	def print_task(self):
		'''prints the name, time_estimate, and depth'''
		print "Name:\t%s" % self.name
		print "Time:\t%d" % self.time_estimate
		print "Depth:\t%d\n" % self.depth

	def print_task_tree(self):
		'''recursively prints the entire root-tree by row'''
		task_list = []
		self.get_task_tree(task_list)

		depth = 0
		count = len(task_list)

		while(count):
			for task in task_list:
				if task.get_depth() == depth:
					task.print_task()
			depth += 1
			count -= 1

def build_task_tree():
	'''query user, build 1 task tree, and return a list of bottom-most subtasks'''
	print "Name of task:"
	name = raw_input("> ")
	print "Estimate amount of time expected for completing \"%s\" (in hours)" % name
	time_estimate = raw_input("> ")

	root = Task(name, time_estimate, 0)

	print "How many parts should \"%s\" be broken into?" % name
	num_subtasks = raw_input("> ")

	#for i in range(0, num_subtasks):
		